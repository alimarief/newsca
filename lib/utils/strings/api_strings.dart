class ApiStrings {
  static const String baseUrl = "http://newsapi.org/v2";
  static const String headlines = "/top-headlines";
  static const String country = "id";
  static const String category = "business";
  static const String apiKey = "81d98da5c83d45a5ad24b6ab1698e745";
}